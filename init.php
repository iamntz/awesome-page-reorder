<?php
/*
Plugin Name: Awesome Page Reorder
Plugin URI:
Description: A smart way to reorder pages without endless waiting times
Author: iamntz
Author URI: http://iamntz.com
Version: 1.0
*/


require_once( 'includes/assets.php' );
require_once( 'includes/ajax.php' );
require_once( 'includes/term-selector.php' );
require_once( 'includes/page-reorder.php' );

new NtzAwesomePageReorder__Assets();
new NtzAwesomePageReorder__Ajax();
new NtzAwesomePageReorder();