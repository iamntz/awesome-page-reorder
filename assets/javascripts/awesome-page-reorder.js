jQuery(document).ready(function($){
  if( typeof APR_ajax_nonce == 'undefined' ){ return; }

  function getItems( el ) {
    $(el).addClass('ajax-activity');

    $.get( ajaxurl, {
      action   : 'awesome_page_reorder_get_items',
      taxonomy : $('select', el).attr('name'),
      term     : $('select', el).val(),
      post_type: $('input', el).val(),
      ajax_nonce : APR_ajax_nonce
    }, addItems);
  }//getItems


  function saveItems() {
    var item     = $('#awesomeReorder').children().filter(':not(.saved)').first();
    var progress = $('.js-save-awesome-order-progress').show();

    var index = item.index();
    progress.html( 'Saving : ' + ( index + 1 ) + ' of ' + $('#awesomeReorder').children().length );

    if( item.length ){
      $.post( ajaxurl, {
        action : 'awesome_page_reorder_item',
        post_id: $('input[name="id"]', item).val(),
        order  : index,
        ajax_nonce : APR_ajax_nonce
      }, function(){
        item.addClass('saved');
        saveItems();
      } );
    }else{
      progress.hide();
      $('#awesomeReorder .saved').removeClass('saved');
    }
  }//saveItems

  $('.js-taxonomy-select select').on('change', function(){
    var val = this.value;
    if( val !== '' ){
      getItems( $(this).parent() );
    }
  });


  $('.js-save-awesome-order').on('click', function(){
    saveItems();
    return false;
  });

  if( !$('.js-taxonomy-select select').length ){
    getItems( '.js-taxonomy-select' );
  }


  $('#awesomeReorder').sortable({
    placeholder: "ui-state-highlight",
    revert: 300,
    forceHelperSizeType: true,
    forcePlaceholderSizeType: true,
    opacity:0.7
  });

  function addItems( data ) {
    $('.js-taxonomy-select').removeClass('ajax-activity');

    $('#awesomeReorder').empty().append( data ).sortable('refresh');
  }//addItems
});