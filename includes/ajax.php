<?php

class NtzAwesomePageReorder__Ajax {
  function __construct() {
    add_action( 'wp_ajax_awesome_page_reorder_get_items', array( &$this, 'items' ) );
    add_action( 'wp_ajax_awesome_page_reorder_item', array( &$this, 'reorder' ) );
  }


  public function items(){
    check_ajax_referer( 'awesome-page-reorder-nonce', 'ajax_nonce' );

    $options = array(
      "post_type"      => sanitize_title( $_GET['post_type'] ),
      "posts_per_page" => -1,
      "order"          => "ASC",
      "orderby"        => "menu_order date",
    );

    if( !empty( $_GET['taxonomy'] ) ){
      $options['tax_query'] = array(
        array(
          "field"    => 'slug',
          "taxonomy" => sanitize_title( $_GET['taxonomy'] ),
          "terms"    => sanitize_title( $_GET['term'] )
        )
      );
    }

    $query = new WP_Query();
    $query->query($options);

    while( $query->have_posts() ){ $query->the_post();
      $content = apply_filters( 'ntz/awesome-page-order/item-content', get_the_title(), get_the_ID() );
      $content .= sprintf( '<input type="hidden" name="id" value="%s" />', get_the_ID() );
      printf( '<div class="awesomeReorder__item">%s</div>', $content );
    }

    die();
  }



  public function reorder(){
    check_ajax_referer( 'awesome-page-reorder-nonce', 'ajax_nonce' );

    wp_update_post( array(
      "ID" => (int) $_POST['post_id'],
      "menu_order" => (int) $_POST['order']
    ) );
  }
}