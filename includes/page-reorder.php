<?php

class NtzAwesomePageReorder {
  function __construct() {
    add_action( 'admin_menu', array( &$this, 'add_menu_items' ), 99 );
  }


  public function add_menu_items(){
    $hierarchical_post_types = get_post_types( array(
      'hierarchical' => true
    ) );

    $this->valid_post_types = apply_filters( 'ntz/awesome-page-order/custom-post-type', $hierarchical_post_types );

    foreach( $this->valid_post_types as $post_type ){
      add_submenu_page( 'edit.php?post_type=' . $post_type, 'Awesome Page Reorder', 'Awesome Page Reorder', 'manage_options',
        'awesome-page-reorder',
        array( &$this, 'page_reorder' ) );
    }
  }


  public function page_reorder(){
    $current_post_type = sanitize_title( $_GET['post_type'] );
    ?>
      <div class="wrap awesomeReorderWrapper">
        <h2>Awesome Page Reorder</h2>
        <p>
          <label>Select Taxonomy:</label>
            <span class="js-taxonomy-select taxonomy-selector">
              <?php new NtzAwesomePageReorder__TermSelector(); ?>
              <input type="hidden" name="post_type" value="<?php echo $current_post_type ?>" />
            </span>
        </p>

        <div id="awesomeReorder">

        </div>

        <span class="button js-save-awesome-order">Save New Order</span>
        <span class="js-save-awesome-order-progress save-awesome-order-progress"></span>
      </div>

      <script type="text/javascript">
        var APR_ajax_nonce = '<?php echo wp_create_nonce( "awesome-page-reorder-nonce" ); ?>';
      </script>

    <?php
  }
}

