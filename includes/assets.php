<?php

class NtzAwesomePageReorder__Assets {
  function __construct() {
    add_action( 'admin_enqueue_scripts', array( &$this, 'assets' ) );
  }


  public function assets(){
    wp_register_style( 'ntz-awesome-page-reorder', plugins_url( '../assets/stylesheets/awesome-page-reorder.css', __FILE__ ), array(), 1, 'screen' );

    wp_register_script( 'ntz-awesome-page-reorder', plugins_url( '../assets/javascripts/awesome-page-reorder.js', __FILE__ ), array(
      "jquery",
      "jquery-ui-sortable",
    ));

    wp_enqueue_script( 'ntz-awesome-page-reorder' );
    wp_enqueue_style( 'ntz-awesome-page-reorder' );

  }
}